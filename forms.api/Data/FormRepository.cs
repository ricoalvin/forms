using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using forms.api.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace forms.api.Data
{
    public class FormRepository : IFormRepository
    {
        private readonly DataContext _context;
        public FormRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<String> GetSubmissionsInRowCol(int formId)
        {
            var submissions = await _context.Submissions.Include(x => x.Answers).Where(x => x.FormId == formId).ToListAsync();
            var form = await _context.Forms.FirstAsync(x => x.Id == formId);

            FormQuestion[] formQuestion = JsonConvert.DeserializeObject<FormQuestion[]>(form.FormJSON);

            Dictionary<int, int> map = new Dictionary<int, int>();
            string[,] grid = new string[submissions.Count() + 1, formQuestion.Count()];

            // We're laying this out like a spreadsheet. Place questions on top row.
            for (int i = 0; i < formQuestion.Count(); i++)
            {
                map.Add(formQuestion[i].id, i);
                grid[0, i] = formQuestion[i].question;
            }

            for (int i = 0; i < submissions.Count(); i++)
            {
                IEnumerator<Answer> enumerator = submissions[i].Answers.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    int mapping = map[enumerator.Current.Qid];
                    grid[i + 1, mapping] = enumerator.Current.Value;
                }
            }

            // Convert to string for CSV
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    sb.Append(grid[i, j] + ",");
                }
                sb.AppendLine();
            }

            return sb.ToString();

        }
    }
}