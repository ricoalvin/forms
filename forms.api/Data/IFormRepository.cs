using System.Threading.Tasks;
using forms.api.Models;

namespace forms.api.Data
{
    public interface IFormRepository
    {
        Task<string> GetSubmissionsInRowCol(int formId);
    }
}