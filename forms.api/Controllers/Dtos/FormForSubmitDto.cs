using System.ComponentModel.DataAnnotations;

namespace forms.api.Controllers.Dtos
{
    public class FormForSubmitDto
    {
        public string title { get; set; }
        public string formJSON { get; set; }
    }
}