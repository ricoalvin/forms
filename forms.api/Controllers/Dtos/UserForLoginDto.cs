using System.ComponentModel.DataAnnotations;

namespace forms.api.Controllers.Dtos
{
    public class UserForLoginDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}