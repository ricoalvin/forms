using System.ComponentModel.DataAnnotations;

namespace forms.api.Controllers.Dtos
{
    public class FormForEditDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string FormJSON { get; set; }
    }
}