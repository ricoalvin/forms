using System.ComponentModel.DataAnnotations;
using forms.api.Models;

namespace forms.api.Controllers.Dtos
{
    public class SubmissionDto
    {
        public int FormId { get; set; }
        public Answer[] answers { get; set; }
    }
}