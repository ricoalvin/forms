﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using forms.api.Controllers.Dtos;
using forms.api.Data;
using forms.api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace forms.api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FormsController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IFormRepository _formRepository;

        public FormsController(DataContext context, IFormRepository formRepository)
        {
            _formRepository = formRepository;
            _context = context;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetValues()
        {
            var values = await _context.Forms.ToListAsync();
            return Ok(values);
        }

        // GET api/values/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetValue(int id)
        {
            var form = await _context.Forms.FirstOrDefaultAsync(x => x.Id == id);
            return Ok(form);
        }

        // GET api/values/5
        [HttpGet("owned")]
        public async Task<IActionResult> GetFormsOfUser()
        {
            var currentUser = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var forms = await _context.Forms.Where(x => x.UserId == currentUser).Select(x => new
            {
                Id = x.Id,
                Title = x.Title
            }).ToListAsync();
            return Ok(forms);
        }

        [HttpGet("csv/{id}")]
        public async Task<IActionResult> GetSubmissions(int id)
        {
            // TODO: not optimized! Form gets queried twice. Once here and once in GetSubmissions... method
            var form = await _context.Forms.FirstOrDefaultAsync(x => x.Id == id);
            if (form.UserId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            string fileName = "results.csv";
            string csvContent = await _formRepository.GetSubmissionsInRowCol(id);

            return File(Encoding.UTF8.GetBytes(csvContent), "text/csv", fileName);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post(FormForSubmitDto value)
        {
            var currentUser = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            Form form = new Form { Title = value.title, FormJSON = value.formJSON, UserId = currentUser };
            await _context.Forms.AddAsync(form);
            await _context.SaveChangesAsync();

            return Ok(new
            {
                id = form.Id
            });
        }

        // PUT api/values/5
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, FormForEditDto value)
        {

            var result = await _context.Forms.SingleAsync(x => x.Id == id);
            if (result != null)
            {
                result.Id = id;
                result.Title = value.Title;
                result.FormJSON = value.FormJSON;
                await _context.SaveChangesAsync();
                return Ok();
            }
            return StatusCode(500);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var form = await _context.Forms.Select(x => new { userId = x.UserId, id = x.Id }).FirstOrDefaultAsync(x => x.id == id);
            if (form.userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();
            Form formToRemove = new Form() { Id = form.id };
            _context.Forms.Attach(formToRemove);
            _context.Forms.Remove(formToRemove);

            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}
