using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using forms.api.Controllers.Dtos;
using forms.api.Data;
using forms.api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace forms.api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SubmissionsController : ControllerBase
    {
        private readonly DataContext _context;

        public SubmissionsController(DataContext context)
        {
            _context = context;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetValues()
        {
            var values = await _context.Forms.ToListAsync();
            return Ok(values);
        }

        // GET api/values/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetValue(int id)
        {
            // var submission = await _context.Submissions.FirstOrDefaultAsync(x => x.Id == id);
            var submission = await _context.Submissions.Include(x => x.Answers).FirstOrDefaultAsync(x => x.Id == id);
            return Ok(submission);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post(SubmissionDto value)
        {
            Submission submit = new Submission { FormId = value.FormId, Answers = value.answers };
            await _context.Submissions.AddAsync(submit);
            await _context.SaveChangesAsync();

            return Ok(new
            {
                id = submit.Id
            });
        }

        // PUT api/values/5
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, FormForEditDto value)
        {

            var result = await _context.Forms.SingleAsync(x => x.Id == id);
            if (result != null)
            {
                result.Id = id;
                result.Title = value.Title;
                result.FormJSON = value.FormJSON;
                await _context.SaveChangesAsync();
                return Ok();
            }
            return StatusCode(500);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
