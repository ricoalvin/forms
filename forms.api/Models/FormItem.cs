namespace forms.api.Models
{
    public class FormItem
    {
        public int id { get; set; }
        public int page { get; set; }
        public bool required { get; set; }
        public string question { get; set; }
        public AnswerItem answer { get; set; }

    }

    public class AnswerItem
    {
        public string type { get; set; }
        public int qid { get; set; }
        public AnswersItem[] answers { get; set; }
    }

    public class AnswersItem
    {
        public int id { get; set; }
        public string value { get; set; }
    }
}