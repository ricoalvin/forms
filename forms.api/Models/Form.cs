namespace forms.api.Models
{
    public class Form
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string FormJSON { get; set; }
    }
}