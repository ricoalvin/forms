namespace forms.api.Models
{
    public class FormQuestion
    {
        public int id { get; set; }
        public bool required { get; set; }
        public string question { get; set; }
    }

}