using System.Collections.Generic;

namespace forms.api.Models
{
    public class Submission
    {
        public int Id { get; set; }
        public int FormId { get; set; }
        public ICollection<Answer> Answers { get; set; }
    }
}