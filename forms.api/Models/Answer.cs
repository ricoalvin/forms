namespace forms.api.Models
{
    public class Answer
    {
        public int Id { get; set; }
        public int Qid { get; set; }
        public string Value { get; set; }
    }
}