import React from 'react';
import styles from '../styles/EndUserForm.module.sass';
import AnswerType from '../utils/AnswerType';
import MultipleChoiceField from './MultipleChoiceField';
import EditableField from './EditableField';

class EndUserFormAnswer extends React.Component {
  componentDidUpdate(prevProps) {}

  handleValidation = item => {
    let isValid = true;
    let isAnswered = item.required ? false : true;

    // First Check for isRequired Validation
    if (!isAnswered) {
      if (
        item.type === AnswerType.checkbox ||
        item.type === AnswerType.multipleChoice
      ) {
        for (let i = 0; i < item.answers.length; i++) {
          if (item.answers[i].checked === true) {
            isAnswered = true;
            break;
          }
        }
      } else {
        if (item.answers[0].value.length > 0) isAnswered = true;
      }
    }

    return isAnswered && isValid;
  };

  handleAnswerChange = (id, newAnswer) => {
    const index = this.props.answers.findIndex(element => element.id === id);
    const answers = [...this.props.answers];
    answers[index] = { id: id, value: newAnswer };
    const isValid = this.handleValidation({ ...this.props, answers: answers });
    this.props.handleChange({
      ...this.props,
      answers: answers,
      isValid: isValid,
      isDirty: true
    });
  };

  handleMultiChoiceAnswerChange = (id, newAnswer, checked = false) => {
    const answers = this.props.answers.map((item, i) => {
      if (item.id === id) return { id: id, value: newAnswer, checked: checked };
      if (this.props.type !== AnswerType.checkbox)
        return { ...item, checked: false };
      return { ...item };
    });
    const isValid = this.handleValidation({ ...this.props, answers: answers });
    this.props.handleChange({
      ...this.props,
      answers: answers,
      isDirty: true,
      isValid: isValid
    });
  };

  renderMultipleChoice = (checkbox = false) => {
    return (
      <React.Fragment>
        {this.props.answers.map((answers, i) => (
          <div
            className={styles.optionContainer}
            key={'multipleChoiceQID' + this.props.qid + '#' + i}
            onClick={() => {
              this.handleMultiChoiceAnswerChange(
                answers.id,
                answers.value,
                !answers.checked
              );
            }}
          >
            <input
              type={checkbox ? 'checkbox' : 'radio'}
              name={`${this.props.qid}`}
              value={answers.value}
              checked={answers.checked || false}
              onChange={e => {
                this.handleMultiChoiceAnswerChange(
                  answers.id,
                  answers.value,
                  e.target.checked
                );
              }}
            />
            <span>{answers.value}</span>
          </div>
        ))}
      </React.Fragment>
    );
  };

  renderShortAnswer = () => {
    return (
      <EditableField
        className={styles.editableField}
        value={this.props.answers[0].value}
        handleChange={e => {
          this.handleAnswerChange(0, e);
        }}
        handleBlur={e => {}}
      />
    );
  };

  renderAnswers = answerType => {
    switch (answerType) {
      case AnswerType.multipleChoice:
        return this.renderMultipleChoice();
      case AnswerType.text:
        return this.renderShortAnswer();
      case AnswerType.checkbox:
        return this.renderMultipleChoice(true);
      default:
        return this.renderMultipleChoice();
    }
  };

  render() {
    return (
      <div className={styles.answerContainer}>
        {this.renderAnswers(this.props.type)}
      </div>
    );
  }
}

export default EndUserFormAnswer;
