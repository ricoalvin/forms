import React from 'react';
import styles from '../styles/FormItem.module.sass';

const FormQuestion = ({ className, ...props }) => {
  return (
    <input
      className={styles['question']}
      type="text"
      value={props.question}
      onChange={e => {
        props.handleChange(e.target.value);
      }}
    />
  );
};

export default FormQuestion;
