import React from 'react';
import AnswerType from '../../utils/AnswerType';
import FormItem from './FormItem';

class Builder extends React.Component {
  constructor(props) {
    super(props);
    this.idBase = -1;

    // const defaultState = sessionStorage.getItem('form')
    //   ? JSON.parse(sessionStorage.getItem('form'))
    //   : this.getDefaultState();

    // const defaultState = FormBuilderService.retrieveForm(
    //   this.props.match.params.formId
    // );

    // this.state = {
    //   currPage: 1,
    //   ...defaultState
    // };

    this.state = this.getDefaultState();
  }

  getDefaultState = () => {
    return {
      entities: {
        items: {
          byId: {
            1: {
              id: 1,
              question: 'Test Question?',
              isRequired: true,
              type: AnswerType.multipleChoice,
              options: ['Option 1', 'Option 2']
            },
            2: {
              id: 2,
              question: 'Test Question?',
              isRequired: false,
              type: AnswerType.multipleChoice,
              options: ['Option 1', 'Option 2']
            }
          },
          allIds: [1, 2]
        }
      }
    };
  };

  handleItemChange = (id, attribute, value) => {
    console.log('Setting State: ' + value);
    this.setState(
      {
        entities: { items: { byId: { [id]: { [attribute]: value } } } }
      },
      () => console.log(this.state)
    );
  };

  render() {
    console.log(this.state);
    return (
      <div>
        {this.state.entities.items.allIds.map(id => {
          const item = this.state.entities.items.byId[id];
          return (
            <FormItem
              question={item.question}
              type={item.type}
              id={item.id}
              options={item.options}
              handleChange={this.handleItemChange}
            />
          );
        })}
      </div>
    );
  }
}

export default Builder;
