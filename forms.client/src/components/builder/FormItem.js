import React from 'react';
import styles from '../../styles/FormItem.module.sass';
import AnswerType from '../../utils/AnswerType';
import PropTypes from 'prop-types';
import EditableField from './EditableField';

class FormItem extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidUpdate() {
    console.log('FormItem updated');
  }

  handleQuestionChange = newQuestion => {
    this.props.handleChange(this.props.id, {
      ...this.props.formItem,
      question: newQuestion
    });
  };

  handleAnswerChange = newAnswer => {
    this.props.handleChange(this.props.id, {
      ...this.props.formItem,
      answer: newAnswer
    });
  };

  handleItemAttributeChange = (attribute, newValue) => {
    this.props.handleChange(this.props.id, {
      ...this.props.formItem,
      [attribute]: newValue
    });
  };

  handleAnswerTypeChange = newType => {
    this.setState({ answerType: newType });
  };

  handleChange = (attribute, newValue) => {
    this.props.handleChange(this.props.id, attribute, newValue);
  };

  render() {
    return (
      <div data-id={this.props.id} className={`${styles.container}`}>
        <div className={`${styles.upperContainer}`}>
          <EditableField
            value={this.props.question}
            handleChange={newValue => this.handleChange('question', newValue)}
          />
        </div>
        <ControlPanel
          required={this.props.isRequired}
          type={this.props.type}
          handleChange={this.handleAnswerTypeChange}
          handleAttributeChange={this.handleItemAttributeChange}
        />
      </div>
    );
  }
}

FormItem.propTypes = {
  question: PropTypes.string,
  type: PropTypes.string,
  id: PropTypes.number,
  options: PropTypes.arrayOf(PropTypes.string)
};

export default FormItem;

class ControlPanel extends React.Component {
  render() {
    return (
      <div className={styles.controlPanel}>
        <button
          onClick={() =>
            this.props.handleAttributeChange('required', !this.props.isRequired)
          }
        >
          {this.props.required ? 'IS REQUIRED' : 'NOT REQUIRED'}
        </button>
        <select
          value={this.props.answerType}
          onChange={e => this.props.handleChange(e.target.value)}
        >
          <option value={AnswerType.multipleChoice}>Multiple Choice</option>
          <option value={AnswerType.text}>Short Answer</option>
        </select>
      </div>
    );
  }
}
