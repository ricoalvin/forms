import React from 'react';

const EditableField = ({ className = ' ', ...props }) => {
  const classes = ['editable-field', className].join(' ');
  const render =
    props.type === 'textarea' ? (
      <textarea
        className={classes}
        value={props.value}
        onChange={e => {
          props.handleChange(e.target.value);
        }}
      />
    ) : (
      <input
        type="text"
        className={classes}
        value={props.value}
        onBlur={e => {
          props.handleBlur(e.target.value);
        }}
        onChange={e => {
          props.handleChange(e.target.value);
        }}
      />
    );

  return render;
};

export default EditableField;
