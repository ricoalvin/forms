import React from 'react';
import FormItem from './FormItem';
import AnswerType from '../utils/AnswerType';
import styles from '../styles/FormBuilder.module.sass';
import FormBuilderService from '../services/FormBuilderService';
import InputKeyUp from './InputOnKeyUp';
import BuilderPostSubmit from './BuilderPostSubmit';
import { Redirect } from 'react-router-dom';

class FormBuilder extends React.Component {
  constructor(props) {
    super(props);
    this.idBase = -1;

    // const defaultState = sessionStorage.getItem('form')
    //   ? JSON.parse(sessionStorage.getItem('form'))
    //   : this.getDefaultState();

    // const defaultState = FormBuilderService.retrieveForm(
    //   this.props.match.params.formId
    // );

    // this.state = {
    //   currPage: 1,
    //   ...defaultState
    // };

    this.state = {
      currPage: 1,
      title: 'New Form',
      items: []
    };

    // this.state = {
    //   currPage: 1,
    //   ...this.getDefaultState()
    // };
  }

  async componentDidMount() {
    let defaultState = {};
    if (this.props.match) {
      let formDto = await FormBuilderService.retrieveForm(
        this.props.match.params.formId
      );
      defaultState = {
        currPage: 1,
        successfulSubmit: false,
        toDashboard: false,
        id: formDto.id,
        title: formDto.title,
        items: JSON.parse(formDto.formJSON)
      };
    } else {
      defaultState = this.getDefaultState();
    }

    this.setState({ currPage: 1, ...defaultState }, () => {
      this.idBase = this.state.items.reduce((highestID, item) => {
        if (item.id > highestID) highestID = item.id;
        return highestID;
      }, 0);
    });
  }

  getDefaultState = () => {
    return {
      title: 'New Form Title',
      successfulSubmit: false,
      toDashboard: false,
      items: [
        {
          id: 1,
          page: 1,
          required: false,
          question: 'Sample Question?',
          answer: {
            type: AnswerType.multipleChoice,
            qid: 1,
            answers: [
              { id: 0, value: 'Option 1' },
              { id: 1, value: 'Option 2' }
            ]
          }
        }
      ]
    };
  };

  generateNewID = () => {
    this.idBase++;
    return this.idBase;
  };

  onFormItemUpdate = (id, newState) => {
    const items = this.state.items.map(item => {
      if (item.id === id) {
        return newState;
      }
      return item;
    });

    this.setState({ items });
  };

  onFormItemDelete = id => {
    console.log('Looking for id: ' + id);
    const items = this.state.items.filter(item => {
      return item.id !== id;
    });
    console.log(items);
    this.setState({ items: items });
  };

  addNewItem = () => {
    const newID = this.generateNewID();
    const defaultItem = {
      id: newID,
      page: this.state.currPage,
      required: false,
      question: 'Question',
      answer: {
        type: AnswerType.text,
        qid: newID,
        answers: [{ id: 0, value: 'O1ption 1' }, { id: 1, value: 'O2ption 2' }]
      }
    };

    this.setState(prevState => ({
      items: [...prevState.items, defaultItem]
    }));
  };

  swapItemPositions = (firstIndex, secondIndex) => {
    console.log(firstIndex + ' ' + secondIndex);
    if (secondIndex < 0) return null;
    if (secondIndex >= this.state.items.length) return null;
    let newItems = [...this.state.items];
    const firstItem = { ...newItems[firstIndex] };

    newItems[firstIndex] = newItems[secondIndex];
    newItems[secondIndex] = firstItem;
    this.setState({ items: newItems }, () => {
      console.log(this.state);
    });
  };

  swapItemPositionsV2 = (id, moveAmount) => {
    const page = this.state.currPage;
    let switchIndex = -1;
    let index = -1;
    let items = [...this.state.items];
    items.forEach((item, i) => {
      if (item.id === id) index = i;
      if (moveAmount < 0) {
        if (item.page === page && index === -1) switchIndex = i;
      } else {
        if (
          item.id !== id &&
          switchIndex === -1 &&
          item.page === page &&
          index !== -1
        )
          switchIndex = i;
      }
    });

    if (switchIndex === -1) return null;

    const secondItem = { ...items[switchIndex] };
    items[switchIndex] = { ...items[index] };
    items[index] = secondItem;

    this.setState({ items: items });
  };

  moveItemToPage = (page, id, appendToEndOfPage = true) => {
    let switchIndex = -1;
    let index = -1;
    let items = [...this.state.items];
    items.forEach((item, i) => {
      if (item.id === id) index = i;
      if (item.page === page && appendToEndOfPage) switchIndex = i;
      if (!appendToEndOfPage && switchIndex === -1 && item.page === page)
        switchIndex = i;
    });

    items[index].page = page;
    if (
      !(appendToEndOfPage && index > switchIndex) &&
      !(!appendToEndOfPage && index < switchIndex) &&
      switchIndex !== -1
    ) {
      const secondItem = { ...items[switchIndex] };
      items[switchIndex] = { ...items[index] };
      items[index] = secondItem;
    }

    this.setState({ items: items });
  };

  saveForm = async () => {
    const { currPage, ...item } = this.state;
    sessionStorage.setItem('form', JSON.stringify(item));

    if (item.id !== undefined) {
      const data = await FormBuilderService.editForm({
        id: item.id,
        title: item.title,
        formJSON: JSON.stringify(item.items)
      });
      if (data) this.setState({ toDashboard: true });
    } else {
      const data = await FormBuilderService.submitForm({
        title: item.title,
        formJSON: JSON.stringify(item.items)
      });
      if (data) this.setState({ id: data.id, successfulSubmit: true });
    }
  };

  handleAttributeChange = (attribute, value) => {
    this.setState({
      [attribute]: value
    });
  };

  render() {
    if (this.state.toDashboard) return <Redirect to="/dashboard" />;
    if (this.state.successfulSubmit)
      return (
        <BuilderPostSubmit formId={this.state.id} title={this.state.title} />
      );

    return (
      <div className={`${styles.container}`}>
        <nav className={`${styles.navbar} ${styles.upperNavbar} navbar`}>
          <div className={`${styles.titleContainer}`}>
            <input
              className={`${styles.formTitle}`}
              type="text"
              value={this.state.title}
              onChange={e =>
                this.handleAttributeChange('title', e.target.value)
              }
            />
            <div className={`${styles.pageIndicator}`}>
              Page {this.state.currPage}
            </div>
          </div>
        </nav>
        <nav className={`${styles.navbar} navbar is-fixed-bottom`}>
          <div className={`${styles.controlPanel}`}>
            <div className={`${styles.rightControlPanel}`}>
              <button
                type="button"
                onClick={() =>
                  this.setState({ currPage: this.state.currPage - 1 })
                }
              >
                Prev Page
              </button>
              <InputKeyUp
                className={`${styles.pageField}`}
                value={this.state.currPage}
                handleKeyUp={e =>
                  this.setState({ currPage: parseInt(e.target.value) })
                }
              />
              <button
                type="button"
                onClick={() =>
                  this.setState({ currPage: this.state.currPage + 1 })
                }
              >
                Next Page
              </button>
            </div>
            <span className={`${styles.divider}`} />
            <div className={`${styles.leftControlPanel}`}>
              <button type="button" onClick={this.addNewItem}>
                Add Question
              </button>
              <button
                type="button"
                onClick={() => {
                  this.swapItemPositions(0, 1);
                }}
              >
                Swap Items
              </button>
              <button
                type="button"
                className={styles.greenButton}
                onClick={this.saveForm}
              >
                Save Form
              </button>
            </div>
          </div>
        </nav>
        <div className={styles.innerContainer}>
          {this.state.items
            .filter(item => {
              return item.page === this.state.currPage;
            })
            .map((item, i, items) => (
              <FormItem
                id={item.id}
                key={item.id}
                formItem={item}
                handleChange={this.onFormItemUpdate}
                handleDelete={() => {
                  this.onFormItemDelete(item.id);
                }}
                handleMove={amt => {
                  if (
                    (amt < 0 && i === 0) ||
                    (amt > 0 && i === items.length - 1)
                  )
                    this.moveItemToPage(
                      this.state.currPage + amt,
                      item.id,
                      amt < 0
                    );
                  else this.swapItemPositionsV2(item.id, amt);
                }}
              />
            ))}
          <div
            className={`${styles.addQuestionContainer}`}
            onClick={this.addNewItem}
          >
            Add New Question
          </div>
        </div>
      </div>
    );
  }
}

export default FormBuilder;
