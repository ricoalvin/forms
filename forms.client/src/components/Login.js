import React from 'react';
import styles from '../styles/Login.module.sass';
import { Redirect } from 'react-router-dom';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isAuthenticated: false
    };
  }

  login = async () => {
    const body = {
      username: this.state.username,
      password: this.state.password
    };

    let response = await fetch('/api/auth/login', {
      method: 'post',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    });
    if (!response.ok)
      console.log('Error with response: ' + response.statusText);
    else {
      let data = await response.json();
      localStorage.setItem('token', data.token);
      this.setState({ isAuthenticated: true });
    }
  };

  handleFieldChanges = (attribute, value) => {
    this.setState({
      [attribute]: value
    });
  };

  render() {
    if (this.state.isAuthenticated) return <Redirect to="/dashboard" />;

    return (
      <div className={styles.container}>
        <div>Username</div>
        <input
          type="text"
          onChange={e => {
            this.handleFieldChanges('username', e.target.value);
          }}
        />
        <div>Password</div>
        <input
          type="password"
          onChange={e => {
            this.handleFieldChanges('password', e.target.value);
          }}
        />
        <button type="button" onClick={this.login}>
          Login
        </button>
      </div>
    );
  }
}

export default Login;
