import React, { useState } from 'react';
import styles from '../styles/FormBuilder.module.sass';
import { Link } from 'react-router-dom';

const BuilderPostSubmit = props => {
  return (
    <div className={styles.successfulSubmitContainer}>
      Your form '<span style={{ fontWeight: 'bold' }}>{props.title}</span>' has
      been submitted successfully.
      <div className={styles.messageContainer}>
        Share Link:-
        <Link to={`/forms/${props.formId}`}>{`${
          process.env.REACT_APP_BASE_URL
        }/forms/${props.formId}`}</Link>
      </div>
      <div className={styles.messageContainer}>
        <Link to={`/dashboard`}>Go to your Dashboard</Link>
      </div>
    </div>
  );
};

export default BuilderPostSubmit;
