import React, { Component } from 'react';
import '../all.sass';
import 'bulma/css/bulma.css';
import FormBuilder from './FormBuilder';
import Builder from './builder/Builder';

class App extends Component {
  render() {
    return <FormBuilder />;
  }
}

export default App;
