import React, { useState } from 'react';
import styles from '../styles/FormItem.module.sass';
import EditableField from './EditableField';

class InputKeyUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      this.setState({ value: this.props.value });
    }
  }

  render() {
    return (
      <input
        className={this.props.className}
        type="text"
        value={this.state.value}
        onKeyUp={e => {
          if (e.keyCode === 13) {
            this.props.handleKeyUp(e);
            this.setState({ value: e.target.value });
          }
        }}
        onChange={e => {
          this.setState({ value: e.target.value });
        }}
      />
    );
  }
}

export default InputKeyUp;
