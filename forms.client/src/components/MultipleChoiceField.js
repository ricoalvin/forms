import React, { useState } from 'react';
import styles from '../styles/FormItem.module.sass';
import EditableField from './EditableField';

const MultipleChoiceField = props => {
  const [hover, setHover] = useState(false);

  return (
    <div
      className={`multiple-choice-field ${props.className} ${
        hover ? styles.selected : ''
      }`}
    >
      <input
        type={props.checkbox ? 'checkbox' : 'radio'}
        name={'qid' + props.name}
        value={props.value}
      />
      <EditableField
        className={styles.editableField}
        value={props.value}
        autofocus={props.autofocus}
        handleChange={props.handleChange}
        handleKeyUp={props.handleKeyUp}
      />
      <button
        type="button"
        className={styles.simpleButton}
        onClick={props.handleDelete}
        onMouseOver={() => {
          setHover(true);
        }}
        onMouseLeave={() => {
          setHover(false);
        }}
      >
        x
      </button>
    </div>
  );
};

export default MultipleChoiceField;
