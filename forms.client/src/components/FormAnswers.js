import React from 'react';
import styles from '../styles/FormItem.module.sass';
import AnswerType from '../utils/AnswerType';
import MultipleChoiceField from './MultipleChoiceField';
import EditableField from './EditableField';

class FormAnswers extends React.Component {
  updateMutableAnswerIDs = answers => {
    //All ids for answer fields will be their index in the containing array
    answers.map((answer, i) => {
      answers[i] = { ...answer, id: i, value: answer.value };
    });
    return answers;
  };

  componentDidUpdate(prevProps) {
    if (prevProps.type !== this.props.type) {
      this.props.handleChange({
        ...this.props,
        answers: [{ id: 0, value: '' }]
      });
    }
  }

  handleAnswerChange = (id, newAnswer) => {
    const index = this.props.answers.findIndex(element => element.id === id);
    const answers = [...this.props.answers];
    answers[index] = { id: id, value: newAnswer };
    this.props.handleChange({ ...this.props, answers: answers });
  };

  handleNewAnswer = () => {
    const newAnswer = { id: -1, value: '', justCreated: true };
    this.props.handleChange({
      ...this.props,
      answers: this.updateMutableAnswerIDs([...this.props.answers, newAnswer])
    });
  };

  handleDeleteChoice = choiceID => {
    this.props.handleChange({
      ...this.props,
      answers: this.props.answers.filter(element => {
        return element.id !== choiceID;
      })
    });
  };

  shouldAutoFocus = answer => {
    if (answer.justCreated !== undefined && answer.justCreated === true)
      return true;
    return false;
  };

  renderMultipleChoice = (checkbox = false) => {
    return (
      <React.Fragment>
        {this.props.answers.map((answers, i) => (
          <div
            key={'multipleChoiceQID' + this.props.qid + '#' + i}
            className={styles.answerContainer}
          >
            <MultipleChoiceField
              key={'multipleChoiceQID' + this.props.qid + '#' + i}
              className={styles.fieldMultipleChoice}
              name={this.props.qid}
              value={answers.value}
              checkbox={checkbox}
              id={answers.id}
              autofocus={this.shouldAutoFocus(answers)}
              handleKeyUp={e => {
                if (e.keyCode == 13) {
                  e.preventDefault();
                  this.handleNewAnswer();
                }
              }}
              handleChange={e => {
                this.handleAnswerChange(answers.id, e);
              }}
              handleDelete={() => this.handleDeleteChoice(answers.id)}
            />
          </div>
        ))}
        <button
          className={styles['addOptionButton']}
          type="button"
          onClick={this.handleNewAnswer}
        >
          Add New Option
        </button>
      </React.Fragment>
    );
  };

  renderShortAnswer = () => {
    return (
      <EditableField
        value={this.props.answers[0].value}
        handleChange={e => {
          this.handleAnswerChange(0, e);
        }}
      />
    );
  };

  renderAnswers = answerType => {
    switch (answerType) {
      case AnswerType.multipleChoice:
        return this.renderMultipleChoice();
      case AnswerType.text:
        return this.renderShortAnswer();
      case AnswerType.checkbox:
        return this.renderMultipleChoice(true);
      default:
        return this.renderMultipleChoice();
    }
  };

  render() {
    return (
      <div className={styles.answersContainer}>
        {this.renderAnswers(this.props.type)}
      </div>
    );
  }
}

export default FormAnswers;
