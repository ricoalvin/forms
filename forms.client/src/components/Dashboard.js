import React from 'react';
import styles from '../styles/Dashboard.module.sass';
import { Link } from 'react-router-dom';
import DashboardService from '../services/DashboardService';
import { Redirect } from 'react-router-dom';

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      forms: [],
      redirect: false
    };
  }

  async componentDidMount() {
    const data = await this.getAllFormsOfUser();
    if (data) this.setState({ forms: data });
  }

  getAllFormsOfUser = async () => {
    let headers = new Headers();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

    let response = await fetch(`/api/forms/owned`, {
      method: 'get',
      headers: headers
    });
    if (!response.ok) {
      if (response.status === 401) this.setState({ redirect: true });
      console.log('Error with request: ' + response.statusText);
    } else {
      let data = await response.json();
      console.log(data);
      return data;
    }
    return null;
  };

  render() {
    if (this.state.redirect) return <Redirect to="/login" />;
    return (
      <div className={styles.container}>
        <h1>Dashboard</h1>
        <div className={styles.innerContainer}>
          <div className={styles.flexContainer}>
            <div className={styles.leftFormItemPanel}>
              <h2 className={styles.headerTwo}>Forms you have created</h2>
            </div>
            <div className={styles.rightFormItemPanel}>
              <Link className={styles.newFormButton} to="/">
                + Create Form
              </Link>
            </div>
          </div>
          <ul>
            {this.state.forms.map(item => {
              return (
                <li className={styles.formItem} key={item.id}>
                  <div className={styles.leftFormItemPanel}>
                    <Link to={`/forms/${item.id}`}>{item.title}</Link>
                  </div>
                  <div className={styles.rightFormItemPanel}>
                    <Link className={styles.button} to={`/form/${item.id}`}>
                      Edit
                    </Link>
                    <button
                      className={styles.button}
                      type="button"
                      onClick={() =>
                        DashboardService.retrieveSubmissions(item.id)
                      }
                    >
                      CSV
                    </button>
                    <button
                      className={`${styles.button} ${styles.deleteButton}`}
                      type="button"
                      onClick={async () => {
                        const result = await DashboardService.deleteForm(
                          item.id
                        );
                        if (result)
                          this.setState({
                            forms: this.state.forms.filter(
                              x => x.id !== item.id
                            )
                          });
                      }}
                    >
                      Delete
                    </button>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}

export default Dashboard;
