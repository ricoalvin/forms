import React from 'react';
import styles from '../styles/FormItem.module.sass';

const EditableField = ({ className = ' ', ...props }) => {
  const attributes = props.autofocus ? { autoFocus: true } : {};
  const classes = ['editableField', className].join(' ');
  const render =
    props.type === 'textarea' ? (
      <textarea
        className={classes}
        value={props.value}
        onChange={e => {
          props.handleChange(e.target.value);
        }}
      />
    ) : (
      <input
        {...attributes}
        type="text"
        className={classes}
        value={props.value}
        onBlur={e => {
          if (props.handleBlue) props.handleBlur(e.target.value);
        }}
        onChange={e => {
          props.handleChange(e.target.value);
        }}
        onKeyUp={e => {
          if (props.handleKeyUp) props.handleKeyUp(e);
        }}
      />
    );

  return render;
};

export default EditableField;
