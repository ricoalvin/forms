import React from 'react';
import styles from '../styles/EndUserForm.module.sass';
import AnswerType from '../utils/AnswerType';
import FormBuilderService from '../services/FormBuilderService';
import EndUserFormAnswer from './EndUserFormAnswer';
import { Redirect } from 'react-router-dom';
import { faRoute } from '@fortawesome/free-solid-svg-icons';

class EndUserForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      successfulSubmission: false,
      currPage: 1,
      ...this.getDefaultState()
    };
  }

  async componentDidMount() {
    const formDto = await FormBuilderService.retrieveForm(
      this.props.match.params.formId
    );

    let items = JSON.parse(formDto.formJSON);

    let defaultState = {
      currPage: 1,
      maxPage: this.findMaxPage(items),
      id: formDto.id,
      title: formDto.title,
      items: JSON.parse(formDto.formJSON)
    };

    defaultState.items.forEach(item => {});

    this.setState({
      currPage: 1,
      ...defaultState
    });
  }

  findMaxPage = items => {
    let maxPage = 0;
    items.forEach(item => {
      if (item.page > maxPage) maxPage = item.page;
    });
    return maxPage;
  };

  getDefaultState = () => {
    return {
      title: 'New Form',
      items: [
        {
          id: 1,
          maxPage: 1,
          page: 1,
          required: false,
          question: 'what?!?',
          answer: {
            type: AnswerType.multipleChoice,
            qid: 1,
            answers: [
              { id: 0, value: 'O1ption 1' },
              { id: 1, value: 'O2ption 2' }
            ]
          }
        }
      ]
    };
  };

  handleSubmissionChange = newState => {
    const items = {
      ...this.state.items.find(item => item.id === newState.qid)
    };
    items.answer = newState;

    const newItemsState = this.state.items.map(item => {
      if (item.id === items.id) return items;
      return item;
    });

    this.setState({ items: newItemsState }, () =>
      console.log(this.state.items)
    );
  };

  handleAttributeChange = (attribute, value) => {
    this.setState({
      [attribute]: value
    });
  };

  handleSubmit = async () => {
    // Parse state into submission dto
    const items = this.state.items;
    let submissionDto = {
      formID: this.state.id,
      answers: []
    };

    items.forEach(item => {
      let answer = '';
      if (
        item.answer.type === AnswerType.checkbox ||
        item.answer.type === AnswerType.multipleChoice
      ) {
        item.answer.answers.forEach(a => {
          answer += a.checked ? a.value + ';' : '';
        });
      } else answer = item.answer.answers[0].value;
      submissionDto.answers.push({ qid: item.answer.qid, value: answer });
      answer = '';
    });

    console.log(submissionDto);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

    let response = await fetch('/api/submissions/', {
      method: 'post',
      body: JSON.stringify(submissionDto),
      headers: headers
    });
    if (!response.ok)
      console.log('Error with response: ' + response.statusText);
    else {
      let data = await response.json();
      console.log(data);
      this.setState({ successfulSubmission: true });
    }
  };

  isCurrentPageValid = () => {
    let flag = true;
    let items = [...this.state.items];
    items.forEach(item => {
      if (item.page === this.state.currPage) {
        if (item.answer.isValid === undefined && item.required) {
          item.answer.isValid = false;
          item.answer.isDirty = true;
          flag = false;
        } else if (!item.answer.isValid && item.required) {
          flag = false;
        }
      }
    });
    this.setState({ items: items });
    console.log(flag);

    return flag;
  };

  changePage = page => {
    if (page > this.state.maxPage || page < 1) return null;

    this.setState({ currPage: page });
  };

  render() {
    return (
      <div className={`${styles.container}`}>
        <div className={`${styles.titleContainer}`}>
          <div className={`${styles.formTitle}`}>{this.state.title}</div>
          <div className={`${styles.pageIndicator}`}>
            {!this.state.successfulSubmission
              ? 'Page ' + this.state.currPage
              : 'Completed!'}
          </div>
        </div>
        {!this.state.successfulSubmission && (
          <div className={`${styles.innerContainer}`}>
            {this.state.items
              .filter(item => {
                return item.page === this.state.currPage;
              })
              .map((item, i) => (
                <EndUserFormItem
                  id={item.id}
                  key={item.id}
                  formItem={item}
                  handleChange={this.handleSubmissionChange}
                />
              ))}
            <div className={styles.controlPanelContainer}>
              <div className={styles.controlPanel}>
                {this.state.currPage > 1 && (
                  <button
                    className={styles.button}
                    type="button"
                    onClick={() => {
                      this.changePage(this.state.currPage - 1);
                    }}
                  >
                    {'< Prev Page'}
                  </button>
                )}
                {this.state.currPage < this.state.maxPage && (
                  <button
                    className={styles.button}
                    type="button"
                    onClick={() => {
                      if (this.isCurrentPageValid())
                        this.changePage(this.state.currPage + 1);
                    }}
                  >
                    {'Next Page >'}
                  </button>
                )}
                {this.state.currPage >= this.state.maxPage && (
                  <button
                    className={styles.submitButton}
                    type="button"
                    onClick={() => {
                      if (this.isCurrentPageValid()) this.handleSubmit();
                    }}
                  >
                    Submit
                  </button>
                )}
              </div>
            </div>
          </div>
        )}
        {this.state.successfulSubmission && <SuccessfulSubmission />}
      </div>
    );
  }
}

const EndUserFormItem = props => {
  const isValidAnswer = answer => {
    if (!answer.isValid && answer.isDirty) return false;
    return true;
  };

  return (
    <div
      className={`${styles.formItem} ${!isValidAnswer(props.formItem.answer) &&
        styles.invalidItem}`}
    >
      <div className={styles.question}>
        {props.formItem.question}
        {props.formItem.required && <span>*</span>}
      </div>
      <EndUserFormAnswer
        type={props.formItem.answer.type}
        qid={props.formItem.id}
        answers={props.formItem.answer.answers}
        required={props.formItem.required}
        handleChange={props.handleChange}
      />
      {
        <div className={styles.validationMessageContainer}>
          {!isValidAnswer(props.formItem.answer) && (
            <span className={styles.validationMessage}>
              This question needs to be answered.
            </span>
          )}
        </div>
      }
    </div>
  );
};

const SuccessfulSubmission = props => {
  return (
    <div className={styles.postSubmissionContainer}>Successful Submission!</div>
  );
};

export default EndUserForm;
