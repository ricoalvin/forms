import React, { useState } from 'react';
import styles from '../styles/FormItem.module.sass';
import FormQuestion from './FormQuestion';
import FormAnswers from './FormAnswers';
import AnswerType from '../utils/AnswerType';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faArrowCircleUp,
  faArrowCircleDown
} from '@fortawesome/free-solid-svg-icons';

class FormItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answerType: this.props.formItem.answer.type,
      deletionWarning: false
    };
  }

  componentDidUpdate() {}

  handleQuestionChange = newQuestion => {
    this.props.handleChange(this.props.id, {
      ...this.props.formItem,
      question: newQuestion
    });
  };

  handleAnswerChange = newAnswer => {
    this.props.handleChange(this.props.id, {
      ...this.props.formItem,
      answer: newAnswer
    });
  };

  handleItemAttributeChange = (attribute, newValue) => {
    this.props.handleChange(this.props.id, {
      ...this.props.formItem,
      [attribute]: newValue
    });
  };

  handleAnswerTypeChange = newType => {
    // let modifiedAnswer = { ...this.props.formItem.answer };
    // modifiedAnswer.type = newType;
    // modifiedAnswer.answers = [{ id: -1, value: 'Short label' }];
    // this.props.handleChange(this.props.id, {
    //   id: this.props.id,
    //   question: this.props.formItem.question,
    //   answer: modifiedAnswer
    // });
    this.setState({ answerType: newType });
  };

  render() {
    return (
      <div data-id={this.props.id} className={`${styles.container}`}>
        <div className={styles.leftContainer}>
          {/* <button type="button" onClick={() => this.props.handleMove(-1)}>
            <span className="upArrowIcon" />
          </button>
          <button type="button" onClick={() => this.props.handleMove(1)}>
            <span className="downArrowIcon" />
          </button> */}
          <FontAwesomeIcon
            className={styles.arrowButton}
            icon={faArrowCircleUp}
            onClick={() => this.props.handleMove(-1)}
          />
          <FontAwesomeIcon
            className={styles.arrowButton}
            icon={faArrowCircleDown}
            onClick={() => this.props.handleMove(1)}
          />
        </div>
        <div
          className={`${styles.rightContainer} ${
            this.state.deletionWarning ? styles.deleteWarn : ''
          }`}
        >
          <div className={styles.header}>
            <FormQuestion
              question={this.props.formItem.question}
              handleChange={this.handleQuestionChange}
            />
            <div className={`${styles.deleteContainer}`}>
              <button
                type="button"
                onClick={this.props.handleDelete}
                onMouseEnter={() => this.setState({ deletionWarning: true })}
                onMouseLeave={() => this.setState({ deletionWarning: false })}
              >
                X
              </button>
            </div>
          </div>
          <div className={`${styles.upperContainer}`}>
            <FormAnswers
              type={this.state.answerType}
              qid={this.props.formItem.answer.qid}
              answers={this.props.formItem.answer.answers}
              handleChange={this.handleAnswerChange}
            />
          </div>
          <ControlPanel
            required={this.props.formItem.required}
            answerType={this.state.answerType}
            handleChange={this.handleAnswerTypeChange}
            handleAttributeChange={this.handleItemAttributeChange}
          />
        </div>
      </div>
    );
  }
}

export default FormItem;

class ControlPanel extends React.Component {
  render() {
    return (
      <div className={styles.controlPanel}>
        <div className={styles.leftControlPanel}>
          <div className={styles.controlPanelButton}>
            <StyledCheckButton
              toggled={this.props.required}
              handleClick={() =>
                this.props.handleAttributeChange(
                  'required',
                  !this.props.required
                )
              }
            />
            <div>Required</div>
          </div>
        </div>
        <div className={styles.rightControlPanel}>
          <select
            className={styles.typeSelector}
            value={this.props.answerType}
            onChange={e => this.props.handleChange(e.target.value)}
          >
            <option value={AnswerType.multipleChoice}>Multiple Choice</option>
            <option value={AnswerType.checkbox}>Checkbox</option>
            <option value={AnswerType.text}>Short Answer</option>
          </select>
        </div>
      </div>
    );
  }
}

const StyledCheckButton = props => {
  return (
    <div
      className={`${styles.styledCheck} ${props.toggled ? styles.toggled : ''}`}
      onClick={() => {
        props.handleClick();
      }}
    />
  );
};
