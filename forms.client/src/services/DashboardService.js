import download from 'downloadjs';

const DashboardService = {
  async retrieveSubmissions(formId) {
    let headers = new Headers();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

    let response = await fetch(`/api/forms/csv/${formId}`, {
      method: 'get',
      headers: headers
    });
    if (!response.ok)
      console.log('Error with response: ' + response.statusText);
    else {
      let data = await response.blob();
      download(data, 'submissions.csv', 'text/csv');
    }
  },

  async deleteForm(formId) {
    let headers = new Headers();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

    let response = await fetch(`/api/forms/${formId}`, {
      method: 'delete',
      headers: headers
    });
    if (!response.ok) {
      console.log('Error with response: ' + response.statusText);
      return false;
    } else {
      console.log('Success in deletion');
      return true;
    }
  }
};

export default DashboardService;
