import download from 'downloadjs';

const FormBuilderService = {
  async submitForm(formState) {
    console.log('Sending over: ');
    console.log(formState);

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

    let response = await fetch('/api/forms/', {
      method: 'post',
      body: JSON.stringify(formState),
      headers: headers
    });
    if (!response.ok) {
      console.log('Error with response: ' + response.statusText);
      return null;
    } else {
      let data = await response.json();
      return data;
    }
  },

  async retrieveForm(formID) {
    let headers = new Headers();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

    let response = await fetch(`/api/forms/${formID}`, {
      method: 'get',
      headers: headers
    });
    if (!response.ok)
      console.log('Error with response: ' + response.statusText);
    else {
      let data = await response.json();
      console.log(data);
      return data;
    }
    return null;
  },

  async editForm(formState) {
    console.log('Sending over: ');
    console.log(formState);

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

    let response = await fetch(`/api/forms/${formState.id}`, {
      method: 'put',
      body: JSON.stringify(formState),
      headers: headers
    });
    if (!response.ok) {
      console.log('Error with response: ' + response.statusText);
      return null;
    } else {
      let data = await response.blob();
      return data;
    }
  },

  async retrieveSubmissions(formId) {
    let headers = new Headers();
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

    let response = await fetch(`/api/forms/csv/${formId}`, {
      method: 'get',
      headers: headers
    });
    if (!response.ok)
      console.log('Error with response: ' + response.statusText);
    else {
      let data = await response.blob();
      download(data, 'submissions.csv', 'text/csv');
    }
  }
};

export default FormBuilderService;
