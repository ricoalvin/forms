const AnswerType = {
  multipleChoice: 'multiple-choice',
  checkbox: 'checkbox',
  text: 'text',
  bigText: 'big-text'
};

export default AnswerType;
