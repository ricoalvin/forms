import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import LoginRoute from './routes/LoginRoute';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import FormBuilder from './components/FormBuilder';
import EndUserForm from './components/EndUserForm';
import Dashboard from './components/Dashboard';

const routes = (
  <BrowserRouter>
    <Switch>
      <Route path="/" component={App} exact={true} />
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/forms/:formId" component={EndUserForm} />
      <Route path="/form/:formId" component={FormBuilder} />
      <Route path="/login" component={LoginRoute} />
    </Switch>
  </BrowserRouter>
);

ReactDOM.render(routes, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
