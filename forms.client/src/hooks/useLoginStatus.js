import React, { useState, useEffect } from 'react';

// TODO: INCOMPLETE
function useLoginStatus() {
  const [isLoggedIn, setIsLoggedIn] = useState(null);

  function handleStatusChange(status) {
    setIsLoggedIn(status);
  }

  useEffect(() => {
    if (localStorage.getItem('token')) handleStatusChange(true);
  });

  return isLoggedIn;
}
